<?php

/**
 * @file
 * Default theme implementation to provide an HTML container for discussion.
 */
?>
<div id="discussion comments" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if ($content['comments']): ?>
    <?php print render($title_prefix); ?>
    <h2 class="title"><?php print t('Discussion'); ?></h2>
    <?php print render($title_suffix); ?>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</div>
