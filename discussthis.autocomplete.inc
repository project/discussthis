<?php

/**
 * @file
 * Function to compute the auto-complete string used in the discussion title.
 */


/**
 * Autocomplete a forum topic discussion title.
 *
 * The function does not return if the auto-complete feature
 * worked. Otherwise, it returns NULL.
 *
 * NOTE :
 * The previous version included a $tid parameter. It was not
 * being used and it would not work properly if the user was
 * creating a new node (i.e. you cannot select a forum, then a
 * title, at least, not dynamically... and it was shown as being
 * like that before.)
 *
 * @param $string The user string so far
 */
function discussthis_autocomplete($string = '') {
  // Anything yet?
  if (!$string) {
    echo drupal_json_encode(array());
    exit();
  }

  // Current user has the right to do that?!
  if (!user_access('access content')) {
    drupal_access_denied();
    return;
  }
  
  $result = db_select('node', 'n')
  ->fields('n', array('nid', 'title'))
  ->condition('type', 'forum', '=')
  ->condition('title', db_like($string) . '%', 'LIKE')
  ->execute();
  
  $matches = array();
  while ($record = $result->fetchAssoc()) {
    $matches[$record['nid']] = check_plain($record['title']) . ' [nid:' . $record['nid'] . ']';
  }
  echo drupal_json_encode($matches);
  exit();
}
