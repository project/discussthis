<?php

/**
 * @file
 *
 * File with node discussion display methods.
 */

/**
 * Implements hook_node_view().
 */
function discussthis_node_view($node, $view_mode, $langcode) {
  global $user;
  
  if ($node) {
    $links = array();

    // Verify that the content type is eligible for discussion.
    $node_types = node_type_get_names();
    $discussthis_types_config = variable_get('discussthis_types_config', array());
    if (!isset($discussthis_types_config[$node->type][$node->type . '_enable']) || !$discussthis_types_config[$node->type][$node->type . '_enable']) {
      return $links;
    }

    // Lookup for topic nid, if it exists (otherwise we get 0).
    $topic_nid = _discussthis_get_topic($node->nid);
    
    // Only pages being displayed full page have comments shown.
    if ($view_mode == 'full' && $topic_nid) {
      $node->content['discussion'] = _discussthis_discussion_load($topic_nid);
      $node->content['discussion']['#weight'] =  $node->content['body']['#weight'] + 100;
      
    }
  }
}


/**
 * Implements hook_node_validate().
 */
function discussthis_node_validate($node, $form) {
  if ($node && user_access('override discuss this forums') && isset($node->discussthis)) {
    if (!empty($node->discussthis['discussthis_topic'])) {
      $topic_nid = $node->discussthis['discussthis_topic'];
      // valid integer?
      // and if valid, is that this very node? if so, that's not good either
      if ($topic_nid == (int) $topic_nid && !is_null($node->nid) && $topic_nid != $node->nid && is_numeric($topic_nid)) {
        // make sure we can actually load that node and that's a forum's node
        $topic = node_load((int) $topic_nid);
        if (!$topic || $topic->type != 'forum') {
          $topic_nid = FALSE;
        }
      }
      else {
        $topic_nid = FALSE;
      }
      if (!$topic_nid) {
        // ugly error setting which works
        form_set_error('discussthis][discussthis_topic', t('The Discuss This! forum topic #@nid was not found (or you chose this very node.) Please, try again. Use the auto-complete feature or enter the exact node identifier. If you did not change the topic identifier, it could have been deleted while you were editing this node. Simply clear the topic entry in that case.', array('@nid' => $node->discussthis['discussthis_topic'])));
        unset($node->discussthis['discussthis_topic']);
      }
    }
    // the forum should never be wrong with a select unless we're working with a hacker
    if (!empty($node->discussthis['discussthis_forum'])) {
      // valid integer? representing a valid forum?
      $forum_tid = $node->discussthis['discussthis_forum'];
      if ((int) $forum_tid != -1) {
        if ($forum_tid == (int) $node->discussthis['discussthis_forum']) {
          $vid = variable_get('forum_nav_vocabulary', '');
          $term = taxonomy_term_load($forum_tid);
          if (!$term || $term->vid != $vid) {
            form_set_error('discussthis][discussthis_forum', t('The Discuss This! forum #@tid was not found.', array('@tid' => $forum_tid)));
            unset($node->discussthis['discussthis_forum']);
          }
        }
        else {
          unset($node->discussthis['discussthis_forum']);
        }
      }
    }
  }
}

/**
 * Implements hook_node_delete().
 */
function discussthis_node_delete($node) {
  // drop any reference to that node
  //
  // PROBLEM: Note that if someone just initiated a "Discuss this"
  // form, then they will have a surprise when saving since the
  // page will be gone. I don't see any way around that though.
  //
  $sql = 'DELETE FROM {discussthis} WHERE nid = %d OR topic_nid = %d';

  db_delete('discussthis')
  ->condition('nid', $node->nid)
  ->execute();
}

// -------------------------------------------------------------------------.
// HELPER METHODS

/**
 * Read the node specific forum settings.
 *
 * Load the Discuss This! data attached to the specified node.
 * For new nodes, load the defaults as defined in the global settings.
 *
 * @param $nid The node identifier, or 0
 * @param $nid $type The type of node
 *
 * @return array
 *          Array structure representing a discussthis_forums row.
 */
function _discussthis_get_forum($nid, $type) {
  
  $forum_fid = db_select('discussthis', 'd')
  ->fields('d', array('forum_tid'))
  ->condition('d.nid', $nid, '=')
  ->execute()
  ->fetchField();
  
  // create defaults if we cannot find data in the table
  if (!$forum_fid) {
    $discussthis_types_config = variable_get('discussthis_types_config', array());
    $forum_fid = isset($discussthis_types_config[$type][$type . '_forum']) ? $discussthis_types_config[$type][$type . '_forum'] : 0;
  }
  return $forum_fid;
}

/**
 * Find forum topics attached to node.
 *
 * Lookup the given nid in the discussthis db table, and return the
 * corresponding forum topic nid, otherwise return the default for
 * this node type
 *
 * @param $nid The node to be checked for a topic identifier.
 *
 * @return integer
 *          The topic nid or 0 if no topic is found
 */
function _discussthis_get_topic($nid) {
  $sql = 'SELECT topic_nid FROM {discussthis} WHERE nid = %d';
  $topic_nid = db_query('SELECT topic_nid FROM {discussthis} WHERE nid = :nid', array(':nid' => $nid))->fetchField();
  return $topic_nid ? $topic_nid : 0;
}

/**
 * Find the number of comments for a node.
 * 
 * @param $nid  the id of the node to retrieve comment count for.
 *
 * @return integer
 *          the comment count.
 */
function _discussthis_comment_num_all($nid) {
  static $cache;

  if (!isset($cache[$nid])) {
    $cache[$nid] = db_query('SELECT comment_count FROM {node_comment_statistics} WHERE nid = :nid', array(':nid' => $nid))->fetchField();
  }
  return $cache[$nid];
}

/**
 * Save a node to forum mapping.
 *
 * This function stores a mapping between the given node (nid) 
 * and a forum (tid). Helps to configure individually each node.
 *
 * @param $discussthis_forum The array to save in the discussthis_forums table
 */
function _discussthis_set_forum($nid, $forum_tid) {
  
  db_merge('discussthis')
  ->key(array('nid' => $nid))
  ->fields(array(
  'nid' =>  $nid,
  'forum_tid' => $forum_tid,
  ))
  ->execute();
  
}

/**
 * Save a node to topic association
 *
 * This function stores a mapping between the given node (nid)
 * and a forum topic (tid). Helps to configure individually each node.
 *
 * @param $discussthis_topic The array to save in the discussthis_forums table
 */
function _discussthis_set_topic($nid, $topic_nid) {
  
  db_merge('discussthis')
  ->key(array('nid' => $nid))
  ->fields(array(
  'nid' =>  $nid,
  'topic_nid' => $topic_nid,
  ))
  ->execute();
}


/**
 * Find discussion comments for a node
 */
function _discussthis_discussion_load($topic_nid) {
  $discussion = array(); 
  $topic = node_load($topic_nid);
  if ($topic) {
    $discussion = comment_node_page_additions($topic);
    unset($discussion['comment_form']);
    $discussion['#theme'] = 'discussion_render_wrapper';
    foreach ($discussion['comments'] as $cid => $comment) {
      if (is_int($cid)) {
       $discussion['comments'][$cid]['#theme'] ='discussion_render_item';
      }
    }
  }
  return $discussion;
    
}
